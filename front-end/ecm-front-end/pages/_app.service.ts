import axios from 'axios';

export const fetchMovies = async () => {
    const url = 'http://localhost:4000/movies';
    const movieData = await axios.get(url).then(response => response);

    return movieData.data;
}
