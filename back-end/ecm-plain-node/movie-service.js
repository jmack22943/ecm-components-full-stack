const axios = require('axios');

const fetchMovies = async () => {
    const handlePromise = new Promise(resolve => {
        const query = `
          query ($id: Int, $page: Int, $perPage: Int, $search: String) {
              Page (page: $page, perPage: $perPage) {
                pageInfo {
                  total
                  currentPage
                  lastPage
                  hasNextPage
                  perPage
                }
                media (id: $id, search: $search) {
                  id
                  title {
                    romaji
                    english
                  }
                }
              }
            }
        `;
        const variables = {
            search: 'Fate/Zero',
            page: 1,
            perPage: 10
        };
        const url = 'https://graphql.anilist.co';
        const options = JSON.stringify({
            query: query,
            variables: variables
        });

        try {
            axios({
                method: 'POST',
                url,
                headers: {
                    'Content-Type': 'application/json',
                    'Accept': 'application/json',
                },
                data: options
            }).then(dataResponse => {

                resolve(dataResponse.data.data);
            });
        } catch (e) {
            resolve(e);
        }
    });

    return await handlePromise.then(moviesResponse => moviesResponse);
}

module.exports = {
    fetchMovies
};
