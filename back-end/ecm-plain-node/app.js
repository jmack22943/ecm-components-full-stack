const express           = require('express');
const app               = express();
const bodyParser        = require('body-parser');
const cors              = require('cors');
const methodOverride    = require('method-override');
const { fetchMovies }   = require('./movie-service');

const APP_URL_PORT  = 4000;

app.use(cors());
app.use(methodOverride());

app.use(bodyParser.urlencoded({limit: '50mb', extended: true, parameterLimit: 50000}));
app.use(bodyParser.json());
app.use((req, res, next) => {
    res.set('X-Powered-By', 'ECM_APP_SERVICE');
    next();
});
app.use((req, res, next) => {
    res.setHeader('Access-Control-Allow-Origin', '*');
    res.setHeader('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
    res.setHeader('Access-Control-Allow-Headers', 'X-Requested-With, content-type, \ Authorization');
    next();
});

app.get('/', (req, res) => {
    res.json({
        success: false,
        message: 'Welcome to the Big Blue!',
        data: {
            color: 'blue',
            title: 'BCBS data'
        },
    });
});

app.get('/movies', async (req, res) => {
    res.json({
        success: true,
        message: 'Movies data.',
        data: await fetchMovies().then(response => response),
    });
});

app.listen(APP_URL_PORT);

console.log(`View the project on: http://localhost:${APP_URL_PORT}`);
