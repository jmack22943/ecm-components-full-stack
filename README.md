ECM Components Project.

FRONT END: Next.js
BACK END: Node.js

Things I would complete if I had more time.
*** Utilize Apollo Server and Apollo Client in order to render out a GraphQL service that allows both projects to easily query and apply mutations for movie titles.
*** You may notice that I scaffolded and Nest.js project that uses TypeScript for the backend service but I did not have time to proper set it up and implement their built in GraphQL module.  I would set that up if more time was allowed.
*** The front end Next.js project has very little component design applied to it.  All that is being done is a mapping of movie titles which render out 10 titles into a Material UI card.
*** Other things:
*** Setup unit tests, create a story book project, apply an Atomic Design patter, setup JWTs on the backend, document the api via swagger/open api, and finally I would containerize the backend service or place it in an AWS Lambda function and expose it via AWS API Gateway.
